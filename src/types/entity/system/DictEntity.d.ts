/**
 * 字典
 * @author GR
 * @date 2023-10-08
 */
declare interface DictEntity extends BaseEntity {
	id: string,
	name: string;
	type: string;
	remark: string;
	status: boolean;
	createAt: string;
	items: DictItemEntity[];
};