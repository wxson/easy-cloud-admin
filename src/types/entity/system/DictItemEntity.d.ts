/**
 * 字典项
 * @author GR
 * @date 2023-10-08
 */
declare interface DictItemEntity extends BaseEntity {
    id?: string,
    /**
     * 字典显示文本
     */
    label: string,
    /**
     * 字典值
     */
    value: string,
    /**
     * 字典项描述
     */
    remark?: string
    /**
     * 字典类型
     */
    dictType: string
};