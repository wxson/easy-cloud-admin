/**
 * 递归获取当前节点的所有父节点
 * @param tree
 * @param targetId
 */
export const TreeNode = {
	/**
	 * 获取所有的父节点ID集合
	 * @param targetId 目标节点ID
	 * @param tree 所有树结构
	 */
	findParentIds(targetId?: string, tree?: any[]): string[] {
		for (let node of tree) {
			// 当前节点为目标节点，停止向下查询
			if (node.id === targetId) {
				return [node.parentId];
			}

			// 存在子节点，继续向下继续查询
			if (node.children && node.children.length > 0) {
				const parentIds = TreeNode.findParentIds(targetId, node.children);
				if (parentIds.length > 0) {
					if (!node.parentId || node.parentId === '0') {
						return parentIds;
					} else {
						return [node.parentId, ...parentIds];
					}
				}
			}
		}

		return [];
	},
};